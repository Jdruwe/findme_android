package be.adevador.findme.activity;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.Button;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.malinskiy.materialicons.Iconify;
import com.squareup.picasso.Picasso;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import be.adevador.findme.R;
import be.adevador.findme.util.IconGenerator;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.activity_login_iv_background)
    ImageView backgroundImage;

    @Bind(R.id.activity_login_btn_facebook_login)
    Button facebookLogin;

    @OnClick(R.id.activity_login_btn_facebook_login)
    @SuppressWarnings("unused") // Injected by Butter Knife
    public void login() {

    }

    private MaterialDialog registrationDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        Picasso.with(this).load(R.drawable.background).fit().centerCrop().into(backgroundImage);

        Drawable facebookIcon = IconGenerator.getIcon(this, Iconify.IconValue.zmdi_facebook);
        facebookLogin.setCompoundDrawablesWithIntrinsicBounds(facebookIcon, null, null, null);
    }

    private void showLegalDialog() {
        registrationDialog = new MaterialDialog.Builder(this)
                .title(R.string.display_activity_login_registration)
                .content(Html.fromHtml(getString(R.string.display_activity_login_finalize_legal)))
                .positiveText(R.string.display_activity_login_agree)
                .negativeText(R.string.display_activity_login_disgree)
                .build();

        registrationDialog.show();
    }

    private void testMqtt() throws MqttException {
        MemoryPersistence persistence = new MemoryPersistence();
        MqttClient client = new MqttClient("tcp://192.168.1.92:1883", "android", persistence);
        MqttConnectOptions connectOptions = new MqttConnectOptions();
        connectOptions.setUserName("alice");
        connectOptions.setPassword("secret".toCharArray());
        client.connect(connectOptions);
        MqttMessage message = new MqttMessage();
        message.setPayload("A single message".getBytes());
        client.publish("androidTopic", message);
        client.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (registrationDialog != null && registrationDialog.isShowing()) {
            registrationDialog.dismiss();
        }
    }

}
