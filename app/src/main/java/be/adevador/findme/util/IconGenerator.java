package be.adevador.findme.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.malinskiy.materialicons.IconDrawable;
import com.malinskiy.materialicons.Iconify;

import be.adevador.findme.R;

public class IconGenerator {
    public static Drawable getIcon(Context context, Iconify.IconValue iconValue) {
        return new IconDrawable(context, iconValue)
                .colorRes(R.color.white)
                .actionBarSize();
    }
}
